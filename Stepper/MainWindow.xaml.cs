﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Stepper
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private int _Result = 5;

        public int Result
        {
            get { return _Result; }
            set
            {
                if(SetValue(ref _Result, value))
                {
                    RaisePropertyChanged("CanIncrease");
                    RaisePropertyChanged("CanDecrease");
                }
            }   
        }
        public bool CanDecrease
        {
            get { return Result > 0; }
        }


        public bool CanIncrease
        {
            get { return Result < 9; }
        }


        public MainWindow()
        {
            DataContext = this;
            InitializeComponent();
        }

        public event PropertyChangedEventHandler PropertyChanged;


        private void OnPlusClick(object sender, RoutedEventArgs e)
        {
            Result++;
        }

        private void OnMoinsClick(object sender, RoutedEventArgs e)
        {
            Result--;
        }

        public bool SetValue<T>(ref T oldValue, T newValue, [CallerMemberName]string propertyName = "")
        {
            if(!EqualityComparer<T>.Default.Equals(oldValue, newValue))
            {
                oldValue = newValue;
                RaisePropertyChanged(propertyName);
                return true;
            }
            return false;
        }

        public void RaisePropertyChanged([CallerMemberName]string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
